# Nano ID

A port of [nanoid](https://github.com/ai/nanoid) to Cangjie

A tiny, secure, URL-friendly, unique string ID generator for Cangjie.

``` cangjie
import nanoid.*

nanoid() //=> "K3YRAK4mK3oPT6Ya84xpT9E22moKnXCa"
nanoid(6) //=> "K3YRAK"
```

